provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "eu-west-1"
}

variable "amis" {
  type = "map"
  default = {
    eu-west-1 = "ami-e1398992"
  }
}

resource "aws_instance" "example" {
  ami           = "${lookup(var.amis, var.region)}"
  instance_type = "t2.micro"
//  vpc_security_group_ids = ["sg-60033607"] //, "default"]
}

resource "aws_eip" "ip" {
    instance = "${aws_instance.example.id}"
}

resource "aws_instance" "another" {
  ami           = "${lookup(var.amis, var.region)}"
  instance_type = "t2.micro"
}


output "ip" {
    value = "${aws_eip.ip.public_ip}"
}

