'use strict';

console.log('Loading event');

exports.handler = function(event, context) {
  console.log('"Hello":"World"');
  context.done(null, {"Hello":"World"});  // SUCCESS with message
};

